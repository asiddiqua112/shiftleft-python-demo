[7m Checking analysis of application `shiftleft-python-demo`. [K[0m

[1mChecking findings on ]8;;https://app.shiftleft.io/apps/shiftleft-python-demo/summary?scan=13\scan 13]8;;\.[0m

Results per rule:

❌report: [1mFAIL[0m
  (22 matched vulnerabilities; configured threshold is 0).

  First 5 findings:

   [1mID[0m  [1mCVSS[0m   [1mRating[0m   [1mTitle[0m                                                    
  ──────────────────────────────────────────────────────────────────────────────
   ]8;;https://app.shiftleft.io/apps/shiftleft-python-demo/vulnerabilities?expanded=true&findingId=18&scan=13\18]8;;\   9.0  [38;2;195;0;0mcritical[0m  Remote Code Execution: Command Injection Through HTTP in…
   ]8;;https://app.shiftleft.io/apps/shiftleft-python-demo/vulnerabilities?expanded=true&findingId=19&scan=13\19]8;;\   9.0  [38;2;195;0;0mcritical[0m  SQL Injection: HTTP Data to SQL Database in `create_user…
   ]8;;https://app.shiftleft.io/apps/shiftleft-python-demo/vulnerabilities?expanded=true&findingId=20&scan=13\20]8;;\   9.0  [38;2;195;0;0mcritical[0m  SQL Injection: HTTP Data to SQL Database in `login`      
   ]8;;https://app.shiftleft.io/apps/shiftleft-python-demo/vulnerabilities?expanded=true&findingId=21&scan=13\21]8;;\   9.0  [38;2;195;0;0mcritical[0m  SQL Injection: HTTP Data to SQL Database in `create_user…
   ]8;;https://app.shiftleft.io/apps/shiftleft-python-demo/vulnerabilities?expanded=true&findingId=22&scan=13\22]8;;\   9.0  [38;2;195;0;0mcritical[0m  SQL Injection: HTTP Data to SQL Database in `login`      

   [1mSeverity rating[0m  [1mCount[0m 
  ────────────────────────
   [38;2;195;0;0mcritical[0m             5 
   [38;2;224;75;73mhigh[0m                 7 
   [38;2;245;166;35mmedium[0m               2 
   [38;2;125;143;153mlow[0m                  2 

   [1mFinding Type[0m  [1mCount[0m 
  ─────────────────────
   Vuln             12 
   Package           6 
   Oss_vuln          4 

   [1mCategory[0m                   [1mCount[0m 
  ──────────────────────────────────
   SQL Injection                  4 
   Security Misconfiguration      2 
   Sensitive Data Exposure        1 
   Remote Code Execution          1 
   Open Redirect                  1 
   Directory Traversal            1 
   Deserialization                1 
   Cross-Site Scripting           1 

   [1mCVE[0m               [1mCount[0m 
  ─────────────────────────
   CVE-2020-28493        1 
   CVE-2019-1010083      1 
   CVE-2019-10906        1 
   CVE-2016-10745        1 

   [1mOWASP 2021 Category[0m                       [1mCount[0m 
  ─────────────────────────────────────────────────
   A03-Injection                                 7 
   A01-Broken-Access-Control                     3 
   A08-Software-And-Data-Integrity-Failures      1 
   A05-Security-Misconfiguration                 1 



